﻿
1-) Web.config dosyasındaki sistem.web tagına aşağıdaki kodu ekleyin
*
<authentication mode="Forms">
      <forms loginUrl="/login/index" timeout="20"></forms> (timeoutta sistemde işlem yapmadan kaç dakika kalıcağını belirtiriz.)
</authentication>
*

2-) Kullanıcı girişi yapılmadan ulaşılmasını istemediğiniz Action veya Controllerların üstüne [Authorize] attribute tanımını ekliyoruz.

3-) [Authorize] eklediimiz controller içindeki herhangi bir action a ulaşılmasını istiyorsak [AllowAnonymous] attribute tanımını kullanabiliriz.

4-) Kullanıcı girişi yaptığını onaylamak için FormsAuthentication.SetAuthCookie(kullanıcıya ait uniq bilgi, şifre beni hatırla);

5-) FormsAuthentication.SetAuthCookie kullanıcıya ait uniq bilgi alanını getirmek için User.Identity.Name kullanılır. Bu bilgi ile kullanıcının diğer bilgileri veritabanından çekilir.

6-) Kullanıcı çıkış yaptığında FormsAuthentication.SignOut(); ile çıkış işlemi gerçekleştirilir.

