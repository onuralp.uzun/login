﻿using Login.Models.Entity;
using Login.Models.VW;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace Login.Controllers
{
    public class LoginController : Controller
    {
        // GET: Login
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Index(VWUser vwuser, string ReturnUrl)
        {
            LoginContext db = new LoginContext();

            ReturnUrl = HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["ReturnUrl"]; //url kısmına verdiğimiz sayfaya login işleminden sonra direk gitmesini sağlar.

            var user = db.Users.FirstOrDefault(x => x.Email == vwuser.Email && x.Sifre == vwuser.Password);

            if (user != null)
            {
                FormsAuthentication.SetAuthCookie(vwuser.Email, vwuser.RememberMe);

                if (ReturnUrl.Length > 0)
                {
                    return Redirect(ReturnUrl);
                }

                return RedirectToAction("Index", "Home");
            }

            TempData["Mesaj"] = "Kullanıcı adı veya şifre yanlış";

            return View(vwuser);
        }
    }
}