﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Login.Models.VW
{
    public class VWUser
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public bool RememberMe { get; set; }
    }
}