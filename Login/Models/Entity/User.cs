﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Login.Models.Entity
{
    public class User
    {
        public int ID { get; set; }
        public string Adi { get; set; }
        public string Soyad { get; set; }
        public string Email { get; set; }
        public string Sifre { get; set; }
    }
}